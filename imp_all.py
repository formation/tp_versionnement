
import os
import imp

print 'starting...'
for root, dirs, files in os.walk(os.getcwd()):
  if '.git' in dirs: dirs.remove('.git')
  for f in files:
    if f == 'imp_all.py': continue
    if f.endswith('.py') :
       imp.load_source(f[:-3],os.path.join(root,f))
print 'done !
